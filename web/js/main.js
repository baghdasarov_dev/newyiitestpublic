$(document).ready(function () {
    $("input[name=balance_button]").click(function () {
        if($(this).parent().parent().find('[name=balance]').val() > 0 ){
            getbalance = $(this).parent().parent().find('[name=balance]').val();
            type = $(this).parent().parent().find('[name=balance]').data('type');
            username = $(this).closest('tr').find(".username_data").data('username');
            oldbalance = $(this).closest('tr').find(".balance_data").data('balance');
            $.ajax({
                type: 'POST',
                url: '/balance',
                data: {balance:getbalance,username:username,oldbalance:oldbalance,type:type},
                success: function(data) {
                    if(data) location.reload();
                    else alert("Перевод не выпылнен попробовайте ещё раз");
                },
            })
        }else{
            alert("Если вы меня увидели то этому причиной явилось то что или вы не ввели ничего или вы ввели минусовую цифру! Спасибо");
        }
    })

    $("input[name=answers]").click(function () {

        answer = $(this).data('answer');
        id = $(this).closest('tr').find(".request").data('id');
        $.ajax({
            type: 'POST',
            url: '/requestAnswer',
            data: {id:id,answer:answer},
            success: function(data) {
                if(data) location.reload();
                else alert("Перевод не выпылнен попробовайте ещё раз");
            },
        })

    })

})
