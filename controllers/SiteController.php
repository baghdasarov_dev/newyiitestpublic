<?php

namespace app\controllers;

use app\models\BalanceForm;
use app\models\Login;
use app\models\TransferForm;
use app\models\User;
use app\models\History;
use app\models\RequestModel;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\ContactForm;



class SiteController extends Controller
{


    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'session'=>true,
        ];
    }


    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $modelLogin = new LoginForm();
        $userModel = new User();

        if (!Yii::$app->session->has('username') && !$modelLogin->load(Yii::$app->request->post())) {
            Yii::$app->session->removeFlash('error_password');
            return $this->render('login', [
                'model' => $modelLogin,
            ]);
        }

        if($userModel->login(Yii::$app->request->post('LoginForm')) === 'error_password'){
            Yii::$app->session->setFlash('error_password','your password write wrong');
            return $this->render('login', [
                'model' => $modelLogin,
            ]);
        }
        elseif(!$userModel->login(Yii::$app->request->post('LoginForm'))){
            $userModel->registrationUser(Yii::$app->request->post('LoginForm'));

        }
        $authUserDate = User::getByUsername(Yii::$app->request->post('LoginForm')['username']);
        Yii::$app->session->set('username',$authUserDate->username);
        Yii::$app->session->set('id',$authUserDate->id);
        Yii::$app->session->set('balance',$authUserDate->balance);
        Yii::$app->session->set('requestCount',
            RequestModel::find()
                ->where(['request_user_id'=>Yii::$app->session->get('id')])
                ->andWhere(['status'=>0])
                ->count()
        );
        return $this->actionDashboard();
    }

    /**
    * Dashboard action.
    *
    * @return string
    */
    public function actionDashboard()
    {
        if (!Yii::$app->session->has('username'))  return $this->actionLogin();
        $allData = User::getAllData(Yii::$app->session->get('username'));
        $modelTransfer = new TransferForm();
        return $this->render('dashboard', [
            'datas' => $allData,
            'modelTrasfer'=>$modelTransfer,
        ]);

    }

    /**
     * Transfer history.
     *
     * @return boolean
     */
    public function actionBalance()
    {
        if (!Yii::$app->session->has('username')) return $this->actionLogin();

        return $this->useBalance(Yii::$app->request->post(),Yii::$app->request->post('type'),Yii::$app->session->get('username'));
    }

    /**
     * Transfer history.
     *
     * @return boolean
     */

    public function useBalance($allData,$dataType,$usernameFromSession){
        $userModel = new User();
        $res = $userModel->getBalance($allData,$dataType);
        if($res){
            $authUserDate = User::getByUsername($usernameFromSession);
            Yii::$app->session->set('balance',$authUserDate->balance);
            return true;
        }
        return false;
    }

    /**
     * Transfer history.
     *
     * @return boolean
     */
    public function actionTransfer()
    {
        if (!Yii::$app->session->has('username')) return $this->actionLogin();

        $resTransfer = $this->useBalance(Yii::$app->request->post('TransferForm'),Yii::$app->request->post('TransferForm')['typeTransfer'],Yii::$app->session->get('username'));
        if($resTransfer){
            return $this->actionDashboard();
        }else{
            return $this->actions();
        }
    }

    /**
     * History history.
     *
     * @return string
     */
    public function actionHistory()
    {
        if (!Yii::$app->session->has('username')) return $this->actionLogin();

        $historyAlls = History::find()
            ->where(['sender_user_id'=>Yii::$app->session->get('id')])
            ->orWhere(['geter_user_id'=>Yii::$app->session->get('id')]);


        $pages = new Pagination(['totalCount' => $historyAlls->count()]);;
        $pages->pageSize = 7;


        $historyAlls = $historyAlls
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(["created_at"=>SORT_DESC])
            ->all();

        foreach ($historyAlls as $key=>$historyAll){
            $historyAlls[$key]->sender_user_id = User::findOne($historyAll->sender_user_id)->username;
            $historyAlls[$key]->geter_user_id = User::findOne($historyAll->geter_user_id)->username;
        }

        return $this->render('history',[
            'historyAlls' => $historyAlls,
            'pages' => $pages,
        ]);
    }

    /**
     * Request action.
     *
     * @return string
     */
    public function actionRequest()
    {
        if (!Yii::$app->session->has('username')) return $this->actionLogin();

        $requestAlls = RequestModel::find()
            ->where(['request_user_id'=>Yii::$app->session->get('id')])
            ->orWhere(['sender_user_id'=>Yii::$app->session->get('id')]);

        $pages = new Pagination(['totalCount' => $requestAlls->count()]);;
        $pages->pageSize = 7;

        $requestAlls = $requestAlls
            ->offset($pages->offset)
            ->limit($pages->limit)
            ->orderBy(["created_at"=>SORT_DESC])
            ->all();

        foreach ($requestAlls as $key=>$requestAll){
            $requestAlls[$key]->username_from = User::findOne($requestAll->request_user_id)->username;
            $requestAlls[$key]->username = User::findOne($requestAll->sender_user_id)->username;
        }

        Yii::$app->session->set('requestCount',
            RequestModel::find()
                ->where(['request_user_id'=>Yii::$app->session->get('id')])
                ->andWhere(['status'=>0])
                ->count()
        );

        return $this->render('request',[
            'requestAlls' => $requestAlls,
            'pages' => $pages,
        ]);
    }

    public function actionRequestanswer()
    {
        if (!Yii::$app->session->has('username')) return $this->actionLogin();

        $userModel = new User();
        $res = $userModel->request(Yii::$app->request->post());
        if($res){
            $authUserDate = User::getByUsername(Yii::$app->session->get('username'));
            Yii::$app->session->set('balance',$authUserDate->balance);
            return true;
        }

        return false;

    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        if(!Yii::$app->session->isActive) Yii::$app->session->open();
        Yii::$app->session->destroy();
        return $this->goHome();
    }

}
