<?php
use yii\helpers\Url as Url;

class HistoryCest
{
    public function ensureThatLoginWorks(AcceptanceTester $I)
    {
        $I->amOnUrl('http://localhost:8080/dashboard');
        $I->see('Login', 'h1');

        $I->amGoingTo('try to login with correct credentials');
        $I->fillField('input[name="LoginForm[username]"]', 'test');
        $I->fillField('input[name="LoginForm[password]"]', 'test123');
        $I->click('login-button');

        $I->expectTo('вы должны видеть историю');
        $I->see('История');

        $I->click('История','a');
        $I->see('Вы находитесь на странице история');
    }
}
