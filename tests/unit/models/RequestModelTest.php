<?php
namespace tests\models;
use app\models\History;
use app\models\RequestModel;
use app\models\User;

class RequestModelTest extends \Codeception\Test\Unit
{
    public function testFindRequestByRequestUserId()
    {
        expect_that($history = RequestModel::findIdentity(100));
        expect($history->_requestUserId)->equals('991');

        expect_not(History::findIdentity(910));
    }

    public function testFindRequestBySenderUserId()
    {
        expect_that($history = RequestModel::findIdentity(100));
        expect($history->_senderUserId)->equals('992');

        expect_not(History::findIdentity(910));
    }

}
