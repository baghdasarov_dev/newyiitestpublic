<?php
namespace tests\models;
use app\models\History;
use app\models\User;

class HistoryTest extends \Codeception\Test\Unit
{
    public function testFindHistoryBySenderUserId()
    {
        expect_that($history = History::findIdentity(100));
        expect($history->senderUserId)->equals('991');

        expect_not(History::findIdentity(910));
    }
    public function testFindHistoryByGeterUserId()
    {
        expect_that($history = History::findIdentity(100));
        expect($history->geterUserId)->equals('992');

        expect_not(History::findIdentity(910));
    }

}
