<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request`.
 */
class m170202_070026_create_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('request', [
            'id' => $this->primaryKey(),
            'request_user_id'=>$this->integer()->notNull(),
            'sender_user_id'=>$this->integer()->notNull(),
            'requestMoney'=>$this->integer()->notNull(),
            'status'=>$this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);

        // creates index for column `request_user_id`
        $this->createIndex(
            'idx-request-sender_user_id',
            'request',
            'request_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-request-request_user_id',
            'request',
            'request_user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-request-request_user_id',
            'request'
        );

        // drops index for column `request_user_id`
        $this->dropIndex(
            'idx-request-request_user_id',
            'request'
        );

        $this->dropTable('request');
    }
}
