<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history`.
 */
class m170131_100254_create_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('history', [
            'id' => $this->primaryKey(),
            'sender_user_id'=>$this->integer()->notNull(),
            'geter_user_id'=>$this->integer()->notNull(),
            'sendMoney'=>$this->integer()->notNull(),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')
        ]);

        // creates index for column `sender_user_id`
        $this->createIndex(
            'idx-history-sender_user_id',
            'history',
            'sender_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-history-sender_user_id',
            'history',
            'sender_user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `geter_user_id`
        $this->createIndex(
            'idx-history-geter_user_id',
            'history',
            'geter_user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-history-geter_user_id',
            'history',
            'geter_user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `history`
        $this->dropForeignKey(
            'fk-history-sender_user_id',
            'history'
        );

        // drops index for column `sender_user_id`
        $this->dropIndex(
            'idx-history-sender_user_id',
            'history'
        );

        // drops foreign key for table `history`
        $this->dropForeignKey(
            'fk-history-geter_user_id',
            'history'
        );

        // drops index for column `history`
        $this->dropIndex(
            'idx-history-geter_user_id',
            'history'
        );

        $this->dropTable('history');
    }
}
