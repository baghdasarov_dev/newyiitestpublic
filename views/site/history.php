<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<h2 class='text-center'>Вы находитесь на странице история</h2>
<hr>
<div class="site-index">
    <table class="table">
        <thead>
            <tr>
                <th>ОН</th>
                <th>Имя Отпрвителя</th>
                <th>Имя Получателя</th>
                <th>Перечисленная Сумма</th>
                <th>Созданна</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($historyAlls as $key=>$date){?>
            <tr>
                <td><?=++$key?></td>
                <td><?=($date->sender_user_id==Yii::$app->session->get('username'))?"<strong>".$date->sender_user_id."</strong>":$date->sender_user_id?></td>
                <td><?=($date->geter_user_id==Yii::$app->session->get('username'))?"<strong>".$date->geter_user_id."</strong>":$date->geter_user_id?></td>
                <td><?=$date->sendMoney?></td>
                <td><?=$date->created_at?></td>

            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?=LinkPager::widget([
        'pagination' => $pages,
    ]);?>
</div>
