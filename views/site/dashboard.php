<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */

$this->title = 'Dashboard';
?>
<div class="site-index">
    <table class="table">
        <thead>
            <tr>
                <th>ОН</th>
                <th>Имя</th>
                <th>Баланс</th>
                <th>Созданна</th>
                <th class="col-md-2 text-center">Отправить сумму</th>
                <th class="col-md-2 text-center">Попросить сумму</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($datas as $key=>$date){?>
            <tr>
                <td><?=++$key?></td>
                <td class="username_data" data-username="<?=$date->username?>"><?=$date->username?></td>
                <td class="balance_data" data-balance="<?=$date->balance?>"><?=$date->balance?></td>
                <td><?=$date->created_at?></td>
                <td>
                    <div class="input-group">
                        <?=Html::input('number','balance','',['class'=>'form-control padding_five','placeholder'=>'Сумма','data-type'=>'0'])?>
                        <div class="input-group-btn">
                            <?=Html::buttonInput('Отправить',['class'=>'btn btn-danger col-md-12','name'=>'balance_button'])?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="input-group">
                        <?=Html::input('number','balance','',['class'=>'form-control padding_five','placeholder'=>'Сумма','data-type'=>'1'])?>
                        <div class="input-group-btn">
                            <?=Html::buttonInput('Попросить',['class'=>'btn btn-success col-md-12','name'=>'balance_button'])?>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <?=Html::buttonInput('Транзакция с новым пользователем',['class'=>'btn btn-info col-md-4 pull-right','data-toggle'=>'modal','data-target'=>'#tranzaktionModal'])?>

    <!-- Modal -->
    <div class="modal fade" id="tranzaktionModal" tabindex="-1" role="dialog" aria-labelledby="tranzaktionModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title"><strong>Транзакция с новым пользователем</strong></span>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                    <div class="modal-body">
                        <div class="input-group">
                            <?php $form = ActiveForm::begin([
                                'id' => 'contact-form',
                                'action'=>'/transfer',
                                'fieldConfig' => [
                                    'template' => "{label}\n<div>{input}</div>\n<div class=\"col-md-12 padding_five text-center\">{error}</div>",
                                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                                ],
                            ]); ?>
                            <div class="col-md-4 padding_five">
                                <?= $form->field($modelTrasfer, 'username')->textInput(['class'=>'form-control','placeholder'=>'Введите имя'])->label(false)?>
                            </div>
                            <div class="col-md-4 padding_five">
                                <?= $form->field($modelTrasfer, 'balance')->textInput(['type' => 'number','class'=>'form-control','placeholder'=>'Сумма'])->label(false)?>
                            </div>
                            <div class="col-md-4 padding_five">
                                <?= $form->field($modelTrasfer, 'typeTransfer')->dropDownList(['2'=>'Выберите тип','0'=>'Отправить','1'=>'Попросить'],['type' => 'number','class'=>'form-control','placeholder'=>'Сумма','autofocus'=>true])->label(false)?>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <?=Html::buttonInput('Отмена',['class'=>'btn btn-secondary','data-dismiss'=>'modal'])?>
                        <?=Html::submitButton('Сделка',['class'=>'btn btn-success'])?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

</div>
