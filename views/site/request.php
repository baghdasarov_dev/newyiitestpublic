<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'Request';
?>
<h2 class='text-center'>Вы находитесь на странице запросы</h2>
<hr>
<div class="site-index">
    <?php if(empty($requestAlls)){ ?>
        <h3 class="text-center">У вас нет запросов</h3>
    <?php }else{?>
        <table class="table">
            <thead>
                <tr>
                    <th>ОН</th>
                    <th>Запрос от</th>
                    <th>Был отправлен</th>
                    <th>Запрашиваемая Сумма</th>
                    <th>Запрос был отправлен</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($requestAlls as $key=>$date){?>
                    <tr>
                        <td><?=++$key?></td>
                        <td class='request' data-id="<?=$date->id?>"><?=($date->username == Yii::$app->session->get('username'))?"<strong>".$date->username."</strong>":$date->username?> </td>
                        <td><?=($date->username_from==Yii::$app->session->get('username'))?"<strong>".$date->username_from."</strong>":$date->username_from?></td>
                        <td><?=$date->requestMoney?></td>
                        <td><?=$date->created_at?></td>
                        <td>

                            <?php if($date->status==0 && $date->username_from == Yii::$app->session->get('username')){?>
                            <div>
                                <?=Html::buttonInput('Отклонить',['class'=>'btn btn-danger col-md-5 marginRdef','name'=>'answers', 'data-answer'=>1])?>
                                <?=Html::buttonInput('Подтвердить',['class'=>'btn btn-success col-md-5 marginRdef','name'=>'answers','data-answer'=>2])?>
                            </div>
                            <?php }elseif($date->status==1){?>
                            <div class="alert alert-danger padding_five text-center">
                                <span>Отклонено</span>
                            </div>
                            <?php }elseif($date->status==2){?>
                            <div class="alert alert-success padding_five text-center">
                                <span>Подтверждено</span>
                            </div>
                            <?php }else{?>
                                <div class="alert alert-info padding_five text-center">
                                    <span>На ваш запрос ещё не ответили</span>
                                </div>
                            <?php }?>

                        </td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
    <?php }?>
    <?=LinkPager::widget([
        'pagination' => $pages,
    ]);?>
</div>
