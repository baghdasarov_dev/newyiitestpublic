<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="/css/bootstrap/bootstrap.css" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
<?php
    NavBar::begin([
        'brandLabel' => 'LOGO',
        'brandUrl' => 'dashboard',
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            [
                'label' => 'Войти',
                'url' => [Url::to('login')],
                'visible' => !Yii::$app->session->has('username')
            ],
            [
                'label' => 'Рабочая среда',
                'url' => [Url::to('dashboard')],
                'visible' => Yii::$app->session->has('username')
            ],
            [
                'label' => 'История',
                'url' => [Url::to('history')],
                'visible' => Yii::$app->session->has('username')
            ],
            [
                'label' => 'Запросты'. Html::tag('span', (Yii::$app->session->get('requestCount')!=0) ? Yii::$app->session->get('requestCount'):'',['class' => 'badge badge-notify']),
                'url' => [Url::to('request')],
                'visible' => Yii::$app->session->has('username'),
                'linkOptions'=>['class'=>'request'],
            ],
            [
                'label' => 'Профил ( '.Yii::$app->session->get('username').' )',
                'items' => [
                    [
                        'label' => 'Мой Баланс ( '.Yii::$app->session->get('balance').' )',
                        'url' => "javascript:void(0)",
                        'visible' => Yii::$app->session->has('balance')
                    ],  
                    '<li class="divider"></li>',
//                    '<li class="dropdown-header">Dropdown Header</li>',

                    [
                        'label' => 'Выйти',
                        'url' => [Url::to('logout')],

                    ],
                ],
                'visible' => Yii::$app->session->has('username')
            ],


        ]

    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= $content ?>
    </div>
</div>


<script src="/js/jquery-2.2.0.min.js"></script>
<script src="/js/main.js"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
