<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class TransferForm extends Model
{
    public $username;
    public $balance;
    public $typeTransfer;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username, balance , typeTransfer are required
            [['username', 'balance','typeTransfer'], 'required','message'=>'Это поле обязательное'],
            ['balance','number','message'=>'Введите цыфроми'],
            ['balance', 'compare', 'compareValue' => 0, 'operator' => '>', 'type' => 'number','message'=>'Выберите положительно число'],
            ['typeTransfer', 'compare', 'compareValue' => 1, 'operator' => '<=', 'type' => 'number','message'=>'Выберите верный тип'],

        ];
    }

}
