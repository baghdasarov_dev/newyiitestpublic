<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class History extends ActiveRecord implements IdentityInterface
{
    public $_id;
    public $authKey;
    public $accessToken;

    /**
    * for test static variable
     */

    public $senderUserId;
    public $geterUserId;
    private static $history = [
        '100' => [
            'id' => '100',
            'senderUserId' => '991',
            'geterUserId' => '992',
            'sendMoney' => '500',
        ],
        '101' => [
            'id' => '101',
            'sender_user_id' => '992',
            'geter_user_id' => '991',
            'sendMoney' => '500',
        ],
    ];


    public static function tableName()
    {
        return 'history';
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$history[$id]) ? new static(self::$history[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
