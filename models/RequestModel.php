<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class RequestModel extends ActiveRecord implements IdentityInterface
{
    public $_id;
    public $authKey;
    public $accessToken;
    public $username;
    public $username_from;

    /**
     * for testing variable
     */
    public $_requestUserId;
    public $_senderUserId;
    public $_requestMoney;
    public $_status;
    public $_sendMoney;

    private static $request = [
        '100' => [
            '_requestUserId' => '991',
            '_senderUserId' => '992',
            '_requestMoney' => '500',
            '_status' => '0',
            '_sendMoney' => '500',
        ],
        '101' => [
            '_requestUserId' => '991',
            '_senderUserId' => '992',
            '_requestMoney' => '500',
            '_status' => '0',
            '_sendMoney' => '500',
        ],
    ];

    public static function tableName()
    {
        return 'request';
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$request[$id]) ? new static(self::$request[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

}
