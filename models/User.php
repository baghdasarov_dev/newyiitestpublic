<?php

namespace app\models;

use app\models\History;
use app\models\RequestModel;
use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    public $_id;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demotest',
            'password' => 'demotest',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    public static function tableName()
    {
        return 'user';
    }

    public function registrationUser($data)
    {
        $user = new User();
        $user->username = $data['username'];
        $user->password = password_hash($data['password'],PASSWORD_DEFAULT);
        return $user->save();
    }

    public function checkAndRegistration($res,$data){
        if($res === null){
            $data['password'] = 'test123';
            $this->registrationUser($data);
        }
        return $this->getByUsername($data['username']);
    }

    public function getBalance($data,$type)
    {
        if($type != 0 && $type != 1) return false;

        $modelHistory = new History();

        $getUserFromBalance = $this->checkAndRegistration($this->getByUsername($data['username']),$data);

        if($type == 0) {
            $getUserFromBalance->balance = $getUserFromBalance->balance + $data['balance'];
            $modelHistory->sender_user_id = Yii::$app->session->get('id');
            $modelHistory->geter_user_id = $getUserFromBalance->id;

            $modelHistory->sendMoney = $data['balance'];
            $modelHistory->save();
            $getUserFromBalance->save();

            $user = User::findOne(['username' => Yii::$app->session->get('username')]);
            $user->balance = $user->balance - $data['balance'];
            return $user->save();
        }
        elseif($type == 1) {
            $requestModel = new RequestModel();
            $requestModel->request_user_id = $getUserFromBalance->id;
            $requestModel->sender_user_id = Yii::$app->session->get('id');
            $requestModel->requestMoney = $data['balance'];
            return $requestModel->save();
        }
        return false;
    }

    public function request($data)
    {
        if($data['answer'] != 1 && $data['answer'] != 2) return false;

        if($data['answer'] == 1){
            $requestModel = RequestModel::findOne(['id',$data['id']]);
            $requestModel->status = $data['answer'];
            return $requestModel->save();
        }elseif ($data['answer'] == 2){

            $requestModel = RequestModel::findOne(['id',$data['id']]);
            $requestModel->status = $data['answer'];

            $request_user_id = $requestModel->request_user_id;
            $sender_user_id = $requestModel->sender_user_id;
            $requestMoney = $requestModel->requestMoney;

            $requestModel->save();

            User::inserHistory($request_user_id,$sender_user_id,$requestMoney);

            User::updateUser(Yii::$app->session->get('id'),$requestMoney,0);
            User::updateUser($sender_user_id,$requestMoney,1);
            return true;

        }
        return false;

    }

    /**
     * $user_id => кого
     * $balance => отпраляемая сумма
     * $type  =>  убавить = 0,прибавить = 1
     * */
    public static function updateUser($user_id,$balance,$type){
        $userModel = User::findOne(['id',$user_id]);
        if($type == 0) $userModel->balance = $userModel->balance - $balance;
        elseif($type == 1) $userModel->balance = $userModel->balance + $balance;
        else return false;

        return $userModel->save();
    }

    /**
     * $sender_user_id = отпрвитель
     * $geter_user_id = получатель
     * $balance = отпраляемая сумма
     * */
    public static function inserHistory($sender_user_id,$geter_user_id,$balance){
        $historyModel = new History();
        $historyModel->sender_user_id = $sender_user_id;
        $historyModel->geter_user_id = $geter_user_id;
        $historyModel->sendMoney = $balance;
        return $historyModel->save();
    }


    public function checkUser($username,$password)
    {
        if(User::findOne(['username' => $username])) {
            $identity = User::findOne(['username' => $username]);
            if(password_verify($password,$identity->password)){
                return Yii::$app->user->login($identity);
            }
            return 'error_password';
        }
        else {
            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login($data)
    {
        $check = $this->checkUser($data['username'],$data['password']);
        if($check === 'error_password') return 'error_password';
        elseif(!$check) return false;
        else return true;
    }

    public static function checkUsernamePasword($data)
    {
        $user = User::findOne(['username' => $data['username']]);
        if($user== null){
            return false;
        }
        return password_verify($data['password'],$user->password);

    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }


    /**
     * Finds user by username
     * for test
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    public static function getByUsername($username)
    {
        return  User::findOne(['username' => $username]);
    }

    public static function getAllData($username){
        return User::find()->where(['<>','username',$username])->orderBy("id")->all();
    }
}
